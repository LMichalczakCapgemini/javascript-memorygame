var controller = (function() {
  var hit = function(event) {
    element = event.target;
    if (element.innerHTML == '') {
      if (model.isFirstPlayer()) {
        view.firstPlayerHit(element);
        view.setSecondPlayer();
      } else {
        view.secondPlayerHit(element);
        view.setFirstPlayer();
      }
    }
  },
  generateElement = function () {
    view.addElement(hit);
  };
  return {
    hit: hit,
    generateElement: generateElement
  }

})();
